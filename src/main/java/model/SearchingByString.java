package model;

import io.cucumber.datatable.dependency.com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SearchingByString {

    /**
     * POJO for Searching by String API Response
     *  Alternative way of saving responses
     */
    private List<Search> Search;

    private String totalResults;

    private String Response;

    public void setSearch(List<Search> Search){
        this.Search = Search;
    }
    public List<Search> getSearch(){
        return this.Search;
    }
    public void setTotalResults(String totalResults){
        this.totalResults = totalResults;
    }
    public String getTotalResults(){
        return this.totalResults;
    }
    public void setResponse(String Response){
        this.Response = Response;
    }
    public String getResponse(){
        return this.Response;
    }
}
