package endpoints;

import env.ApplicationProperties;
import env.Environment;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import model.SearchingByString;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.runner.Request;
import org.openqa.grid.web.servlet.handler.RequestType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BaseEndPoints {
    ApplicationProperties appProps = Environment.INSTANCE.getApplicationProperties();



    /**
     * common specification for request
     */
    public RequestSpecification getCommonSpec() {
        RequestSpecification rSpec = SerenityRest.given();
        rSpec.contentType(ContentType.JSON).baseUri(appProps.getBaseURL());
        return rSpec;
    }




    /**
     * Verify that the response code is the same as expected code by comparing the
     * provided expected code and the response code from the response received by
     * sending the request
     */
    public void verifyResponseStatusCode(Response response, int expectedCode) {
        Assert.assertEquals(response.getStatusCode(), expectedCode);
    }


    /**
     * Method for making request to get By Movie ID method
     */



    public Response callEndpointHttpMethodGetById(String value) {
        Response response = SerenityRest.given()
                .contentType("application/json")
                .header("Content-Type", "application/json").queryParam("i",value)
                .when().
                get(appProps.getBaseURL());
        return response;

    }

    /**
     * Method for making request to get By  Movie Title method
     */


    public Response callEndpointHttpMethodGetByTitle(String value) {
        Response response = SerenityRest.given()
                .contentType("application/json")
                .header("Content-Type", "application/json").
                queryParam("t",value)
                .when().
                get(appProps.getBaseURL());
        return response;

    }

    /**
     * Method for making request to get By Movie Name  method
     */


    public  Response callEndpointHttpMethodGetByName(String value, int i) {
        Response response = SerenityRest.given()
                .contentType("application/json")
                .header("Content-Type", "application/json").queryParams("s",value).queryParam("page",i).when().
                get(appProps.getBaseURL());
        return response;

    }



    /**
     * Method to convert String value to Int
     */


    public int stringToInt(String total){
        return  Integer.parseInt(total);
    }




}








