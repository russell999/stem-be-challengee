package steps;


import endpoints.BaseEndPoints;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import io.restassured.response.Response;
import model.Search;
import model.SearchingByString;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.*;



public class SearchingSteps {


    BaseEndPoints basemethods = new BaseEndPoints();

    List<String> movies = new ArrayList<>();

    Builders build = new Builders();

    List<Map<String, String>> searchResults ;
    List<Map<String,String>> listOfMoviesData;

    @When("I search movie by name {string} i should save all of the results and i should verify that result {string} at least {int} movies")
    public void iSearchMovieByNameIShouldSaveAllOfTheResultsAndIShouldVerifyThatResultAtLeastMovies(String value, String totalResults, int num) {
        int i = 0;


     listOfMoviesData = new ArrayList<Map<String,String>>();  // we need this List<Map> to store list of result

                while (true ) {
            i++;   // we need this variable to handle pagination, Each execution it will increase by 1
                    // And we will make call to the new page


                // we need this method to triger Search by Name endpoint
         build.setResponse(basemethods.callEndpointHttpMethodGetByName(value,i));


                /**
                 * In order to stop while loop condition should be false
                 * As soon we will reach final page Content length header will occur and  we will exit the loop
                 */
                if (build.getResponse().getHeaders().hasHeaderWithName("Content-Length")) {
                    break;
                }

      //We will save all the Nested Json Search data inside of searchResult Method
            searchResults= build.getResponse().jsonPath().get("Search");

            listOfMoviesData.addAll(searchResults);

            // We need this loop to store all of the movies inside of List of Movies to further validate it
                for (Map<String, String> searchResult : searchResults) {
                    movies.add(searchResult.get("Title"));
                    }
        }
    }

    @Then("I should verify that following Movies are presented {string} {string}")
    public void iShouldVerifyThatFollowingMoviesArePresented(String movie, String movie2) {
     Assert.assertTrue(movies.contains(movie));
        Assert.assertTrue(movies.contains(movie2));
    }

    @When("Trigger get_by_id method to get details of this {string} movie from above list")
    public void trigger_get_by_id_method_to_get_details_of_this_movie_from_above_list(String string) {
String imd= "";

// Loop to get imdbID details based on Title movie
        for (int i = 0; i < listOfMoviesData.size(); i++) {
            if(listOfMoviesData.get(i).get("Title").equals(string)){
              imd +=listOfMoviesData.get(i).get("imdbID");
            }
        }

        build.setResponse(basemethods.callEndpointHttpMethodGetById(imd));

    }


    @And("Verify that  movie {string} at {string} and {string} by {string}")
    public void verifyThatMovieAtAndBy(String Released, String releasedValue, String Director, String directorValue) {
        Assert.assertEquals(build.getResponse().jsonPath().get(Released), releasedValue);
        Assert.assertEquals(build.getResponse().jsonPath().get(Director), directorValue);
    }

    @When("Trigger getByTitle endpoint with {string} value")
    public void triggerGetByTitleEndpointWithValue(String title) {

        build.setResponse(basemethods.callEndpointHttpMethodGetByTitle(title));

    }

    @Then("Verify plot has value {string} and has runtime {string}")
    public void verifyPlotHasValueAndHasRuntime(String expectedPlotValue, String expectedRunTimeValue) {

        // All of this values hardcoded but we also can make it dynamic based on User requirments . example of it above
        // with Director value
      String actualPlotValue= build.getResponse().jsonPath().get("Plot");

        String actualRunTimeValue= build.getResponse().jsonPath().get("Runtime");


        Assert.assertTrue(actualPlotValue.toLowerCase().contains(expectedPlotValue.toLowerCase()));
        Assert.assertTrue(actualRunTimeValue.equalsIgnoreCase(expectedRunTimeValue));


    }

}

