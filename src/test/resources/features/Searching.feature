Feature: OMDb API

  Scenario: Search by String and return list of Results
    When I search movie by name "stem" i should save all of the results and i should verify that result "totalResults" at least 30 movies
    Then I should verify that following Movies are presented "The STEM Journals" "Activision: STEM - in the Videogame Industry"
    Then Trigger get_by_id method to get details of this "Activision: STEM - in the Videogame Industry" movie from above list
    And  Verify that  movie "Released" at "23 Nov 2010" and "Director" by "Mike Feurstein"
     And Trigger getByTitle endpoint with "The STEM Journals" value
      And  Verify plot has value "Science, Technology, Engineering and Math" and has runtime "22 min"
